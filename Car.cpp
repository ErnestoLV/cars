#include "Car.h"


Car::Car()
{
    model = "";
    year = "";
    type = "";
}

Car::Car(string mod, string yea, string typ)
{
    model = mod;
    year = yea;
    type = typ;
}

string Car::getSpecs()
{
    return "Model: " + model + " Year: " + year + " Type: " + type;
}

void Car::setSpecs( string mod, string yea, string typ)
{
    model = mod;
    year = yea;
    type = typ;
}

 Tire Car::getTireByIndex(int index)
 {
     return Tires[index];
 }

 void Car::setPressureByIndex(int index, int pres)
 {
     Tires[index].setPressure(pres);
 }
