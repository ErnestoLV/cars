#include <iostream>
#include "Tire.h"
#include "Car.h"


using namespace std;

int main()
{
    Car myCar("A4", "2020", "Coupe");
    Car myCar2();
    Car myCar3();

    cout << myCar.getSpecs() << endl;

    Tire tire = myCar.getTireByIndex(0); // nope
    cout << tire.getSpecs()<< endl;      //Nope

    cout << myCar.getTireByIndex(0).getSpecs() << endl;

    myCar.setPressureByIndex(1,40);


    cout << myCar.getTireByIndex(0).getPressure() << endl;
    cout << myCar.getTireByIndex(1).getPressure() << endl;

    return 0;
}
